package scripts.fc.fcplankmaker.data;

public enum PlankType
{
	NORMAL(1511, 960, 100, 101),
	OAK(1521, 8778, 250, 107),
	TEAK(6333, 8780, 500, 112),
	MAHOGANY(6332, 8782, 1500, 117);
	
	private int logId;
	private int plankId;
	private int productionPrice;
	private int interfaceChild;
	
	PlankType(int logId, int plankId, int productionPrice, int interfaceChild)
	{
		this.logId = logId;
		this.plankId = plankId;
		this.productionPrice = productionPrice;
		this.interfaceChild = interfaceChild;
	}
	
	public int getLogId()
	{
		return this.logId;
	}
	
	public int getPlankId()
	{
		return this.plankId;
	}
	
	public int getProductionPrice()
	{
		return this.productionPrice;
	}
	
	public int getInterfaceChild()
	{
		return this.interfaceChild;
	}
}
