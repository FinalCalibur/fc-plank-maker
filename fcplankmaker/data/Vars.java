package scripts.fc.fcplankmaker.data;

public class Vars
{
	private static Vars vars;

	public static Vars get()
	{
		return vars == null ? vars = new Vars() : vars;
	}
	
	public static void reset() 
	{
		vars = null;
	}
	
	public PlankType 	plankType; //This is the plank type we will be making --> selected by user in GUI
	public boolean		hasSupplies = true; //This will be the condition we use in our main while loop - Script ends when this is false
	public String 		status = "STARTING UP"; //Status for paint
}
