package scripts.fc.fcplankmaker.workers;

import scripts.fc.fcplankmaker.workers.impl.Bank;
import scripts.fc.fcplankmaker.workers.impl.GoToMill;
import scripts.fc.fcplankmaker.workers.impl.MakePlanks;
import scripts.fc.workerframework.Worker;

public enum Workers
{
	BANK(new Bank()),
	GO_TO_MILL(new GoToMill()),
	MAKE_PLANKS(new MakePlanks());
	
	private Worker worker;
	
	Workers(Worker worker)
	{
		this.worker = worker;
	}
	
	public Worker getWorker()
	{
		return this.worker;
	}
}
