package scripts.fc.fcplankmaker.workers.impl;

import org.tribot.api.General;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.WebWalking;

import scripts.fc.fcplankmaker.data.Vars;
import scripts.fc.fcplankmaker.workers.Workers;
import scripts.fc.workerframework.Worker;

public class Bank implements Worker
{
	private final int GP_ITEM_ID = 995; //Item ID for "Coins"
	
	@Override
	public void work()
	{
		Vars.get().status = "BANK";
		
		if(!Banking.isInBank()) //IF(we are not in the bank)
		{
			WebWalking.walkToBank(); //Use WebWalking to walk to the bank
		}
		else //ELSE(we are in the bank)
		{
			if(Banking.openBank()) //IF(we have opened the bank)
			{
				General.sleep(600, 1200); //Little sleep so bank can load
				
				if(depositPlanks()) //IF(we have deposited planks in the bank)
				{
					withdrawMaterials(); //withdraw the required materials
				}
			}
		}
	}

	@Override
	public Worker getNextWorker()
	{
		if(hasMaterials()) //IF(we have materials (logs & coins))
		{
			return Workers.GO_TO_MILL.getWorker(); //We should now go to the mill, so return the appropriate worker
		}
		
		return null; //Otherwise we don't have materials, so return null (meaning this worker will execute again)
	}
	
	private boolean depositPlanks()
	{
		//True if we've deposited items, OR if our inventory is not full
		return Banking.depositAllExcept(GP_ITEM_ID, Vars.get().plankType.getLogId()) > 0 || !Inventory.isFull();
	}
	
	private void withdrawMaterials()
	{
		//IF(we don't have enough gp in inventory OR don't have any more logs in bank and inventory)
		if((Inventory.getCount(GP_ITEM_ID) < Vars.get().plankType.getProductionPrice())
				|| (Banking.find(Vars.get().plankType.getLogId()).length == 0 && Inventory.getCount(Vars.get().plankType.getLogId()) == 0))
		{
			General.println("Player does not have enough supplies to continue production. Ending script...");
			
			Vars.get().hasSupplies = false;
		}
		else
		{
			Banking.withdraw(0, Vars.get().plankType.getLogId()); //We still have supplies to continue. Withdraw logs from bank
		}
	}
	
	private boolean hasMaterials()
	{
		//Return whether or not we have enough gold and logs in our inventory to make planks
		return Inventory.getCount(GP_ITEM_ID) >= Vars.get().plankType.getProductionPrice()
				&& Inventory.getCount(Vars.get().plankType.getLogId()) > 0;
	}

}
