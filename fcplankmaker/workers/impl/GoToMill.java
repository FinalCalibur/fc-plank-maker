package scripts.fc.fcplankmaker.workers.impl;

import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSTile;

import scripts.fc.fcplankmaker.data.Vars;
import scripts.fc.fcplankmaker.workers.Workers;
import scripts.fc.workerframework.Worker;

public class GoToMill implements Worker
{
	private final RSArea MILL_AREA = new RSArea(new RSTile(3300, 3491, 0), new RSTile(3304, 3489, 0));
	
	@Override
	public void work()
	{
		Vars.get().status = "GO TO MILL";
		
		WebWalking.walkTo(MILL_AREA.getRandomTile()); //Web walk to a random tile in our defined MILL_AREA
	}

	@Override
	public Worker getNextWorker()
	{
		if(isAtMill()) //IF(we are at the mill)
		{
			//We should now make planks, so return the appropriate worker
			return Workers.MAKE_PLANKS.getWorker();
		}
		
		//Otherwise we are not at the mill, so return null, meaning this worker will execute again
		return null;
	}
	
	private boolean isAtMill()
	{
		//Return whether or not our defined MILL_AREA contains the player's current position
		return MILL_AREA.contains(Player.getPosition());
	}

}
