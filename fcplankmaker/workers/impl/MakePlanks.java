package scripts.fc.fcplankmaker.workers.impl;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Camera;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.types.RSInterfaceChild;
import org.tribot.api2007.types.RSNPC;

import scripts.fc.fcplankmaker.data.Vars;
import scripts.fc.fcplankmaker.workers.Workers;
import scripts.fc.workerframework.Worker;

public class MakePlanks implements Worker
{
	private final String 	NPC_NAME = "Sawmill operator"; //The NPC name for the sawmill operator --> Will be used to find the NPC at first, and then we will cache the ID
	private final int 		BUY_INTERFACE_MASTER = 403; //The master interface ID for the buy planks interface
	private final Condition BUY_INTERFACE_CONDITION = buyInterfaceCondition(); //Condition to tell us whether or not the buy interface is up
	private final Condition BUY_PLANKS_CONDITION = buyPlanksCondition(); //Condition to tell us whether or not we have bought planks successfully
	
	private int 	npcId = -1; //Cache for the sawmill operator's npc id
	private RSNPC 	operator; //The operator NPC
	
	@Override
	public void work()
	{
		/*
		 * TLDR --> Lack of null / length checking in this worker is due to an experiment with a different error handling strategy.
		 * 
		 * I decided to experiment with this worker.
		 * 
		 * Instead of doing the classic null / array length checks for everything, I'm trying out a different
		 * development principle.
		 * 
		 * Classic methodology is LBYL (Look before you leap) --> This means you validate data before using it (ex. null checking or length checking arrays)
		 * 
		 * I've decided to try and use EAFP (Easier to ask forgiveness than permission) for this worker --> This means you use exceptions to catch and handle
		 * errors as they occur.
		 * 
		 * EAFP can be nice to use when you don't actually expect any issues to arise (or if you do, you expect them rarely).
		 * 
		 * So if any null pointers / index out of bounds exceptions get raised, the try / catch block will catch it and the script will continue
		 * as normal.
		 * 
		 * For beginning scripters, I still recommend using LBYL as data validation is a good habit to have. I just decided to experiment with EAFP
		 * out of curiosity.
		 * 
		 * For more info, look here: http://www.oranlooney.com/lbyl-vs-eafp/
		 */
		try
		{
			Vars.get().status = "MAKE PLANKS";
			
			operator = getOperator(); //Get the operator NPC
			
			if(isBuyInterfaceUp()) //IF(buy interface is currently up)
			{
				buyPlanks(); //Buy the planks
			}
			else //ELSE(buy interface is not currently up)
			{		
				adjustCamera(); //Adjust the camera if necessary (so we can click the NPC)
				
				openBuyInterface(); //Open up buy interface
			}
		}
		catch(Exception e)
		{
			General.println("Easier to ask forgiveness than permission...");
			
			e.printStackTrace();
		}
	}

	@Override
	public Worker getNextWorker()
	{
		if(hasPlanks()) //IF(we have planks)
		{
			//We should bank them now, so choose the appropriate worker
			return Workers.BANK.getWorker();
		}
		
		//Otherwise we don't have planks yet, so return null, meaning this worker will execute again
		return null;
	}
	
	private boolean isBuyInterfaceUp()
	{
		//Return whether or not the buy interface is up
		return Interfaces.get(BUY_INTERFACE_MASTER) != null;
	}
	
	private void buyPlanks()
	{
		//Get the appropriate interface child for the chosen plank type
		RSInterfaceChild buyInterface = Interfaces.get(BUY_INTERFACE_MASTER, Vars.get().plankType.getInterfaceChild());
		
		if(buyInterface.click("Buy All")) //IF(we successfully clicked the Buy All option on the buy interface)
		{	
			//Wait until the buy request goes through, or a 2-3.5 second timeout if it didn't
			Timing.waitCondition(BUY_PLANKS_CONDITION, General.random(2000, 3500));
		}
	}
	
	private void openBuyInterface()
	{
		if(DynamicClicking.clickRSNPC(operator, "Buy-plank")) //IF(we successfully clicked the Buy-plank option on the sawmill operator)
		{
			//Wait until the buy interface is up, or a timeout of 1.5-2 seconds
			Timing.waitCondition(BUY_INTERFACE_CONDITION, General.random(1500, 2000));
		}
	}
	
	private void adjustCamera()
	{
		if(!operator.isClickable()) //IF(the sawmill operator is not clickable)
		{
			Camera.turnToTile(operator); //Turn the camera to the sawmill operator
		}
	}
	
	private boolean hasPlanks()
	{
		//Return whether or not our inventory contains planks
		return Inventory.getCount(Vars.get().plankType.getLogId()) == 0;
	}
	
	private RSNPC getOperator()
	{
		/*
		 * Warning -
		 * 
		 * This method does not contain array length checking
		 * 
		 * For those following LBYL (explained above), always length check your arrays
		 */
		
		if(npcId != -1) //IF(we have our npc id cached)
		{
			//Search for sawmill operator NPC by it's ID and return it
			return NPCs.find(npcId)[0];
		}
		else //ELSE(we do not have our npc id cached)
		{
			RSNPC npc = NPCs.find(NPC_NAME)[0]; //Find the sawmill operator by it's name
			
			npcId = npc.getID(); //Cache the ID
			
			return npc; //return the npc
		}
	}
	
	private Condition buyInterfaceCondition()
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				General.sleep(100, 200);
				
				return isBuyInterfaceUp();
			}		
		};
	}
	
	private Condition buyPlanksCondition()
	{
		return new Condition()
		{
			@Override
			public boolean active()
			{
				General.sleep(100, 200);
				
				return hasPlanks();
			}	
		};
	}

}
