package scripts.fc.fcplankmaker.gui;

import javax.swing.JFrame;

import scripts.fc.fcplankmaker.data.PlankType;
import scripts.fc.fcplankmaker.data.Vars;

public class FCPlankMakerGUI extends JFrame
{	
	private static final long serialVersionUID = -6133874132884970679L; //Eclipse gives a warning if this isn't here --> Don't worry about this

	public FCPlankMakerGUI()
	{	
		initComponents();
	}

	/*
	 * Used NetBeans to design GUI, and it automatically generates this method.
	 * 
	 * Highly recommend NetBeans for fast and simple GUI design
	 */
	private void initComponents()
	{
		typeBox = new javax.swing.JComboBox<String>();
		typeLabel = new javax.swing.JLabel();
		titleLabel = new javax.swing.JLabel();
		startButton = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("FC Plank Maker");
		setResizable(false);

		typeBox.setModel(new javax.swing.DefaultComboBoxModel<String>(new String[] {
				"Normal", "Oak", "Teak", "Mahogany", " " }));

		typeLabel.setText("Type:");

		titleLabel.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
		titleLabel.setText("FC Plank Maker");

		startButton.setText("Start");
		startButton.addActionListener(new java.awt.event.ActionListener()
		{
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				startButtonActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap(44, Short.MAX_VALUE)
								.addComponent(titleLabel)
								.addContainerGap(43, Short.MAX_VALUE))
				.addGroup(
						javax.swing.GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup()
								.addContainerGap(
										javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)
								.addComponent(typeLabel)
								.addGap(10, 10, 10)
								.addComponent(typeBox,
										javax.swing.GroupLayout.PREFERRED_SIZE,
										javax.swing.GroupLayout.DEFAULT_SIZE,
										javax.swing.GroupLayout.PREFERRED_SIZE)
								.addGap(25, 25, 25))
				.addGroup(
						layout.createSequentialGroup()
								.addGap(57, 57, 57)
								.addComponent(startButton)
								.addContainerGap(
										javax.swing.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)));
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						javax.swing.GroupLayout.Alignment.TRAILING,
						layout.createSequentialGroup()
								.addContainerGap()
								.addComponent(titleLabel)
								.addGap(18, 18, 18)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.BASELINE)
												.addComponent(
														typeBox,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(typeLabel))
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED,
										25, Short.MAX_VALUE)
								.addComponent(startButton)));

		pack();
	}// </editor-fold>

	private void startButtonActionPerformed(java.awt.event.ActionEvent evt)
	{
		//Set plankType object to chosen plank type in GUI
		Vars.get().plankType = PlankType.valueOf(typeBox.getSelectedItem().toString().toUpperCase());
		
		//Get the GUI off the screen
		dispose();
	}

	private javax.swing.JButton startButton;
	private javax.swing.JLabel titleLabel;
	private javax.swing.JComboBox<String> typeBox;
	private javax.swing.JLabel typeLabel;
}
