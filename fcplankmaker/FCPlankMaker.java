package scripts.fc.fcplankmaker;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Toolkit;

import org.tribot.api2007.Login;
import org.tribot.api2007.Login.STATE;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Ending;
import org.tribot.script.interfaces.Painting;
import org.tribot.script.interfaces.Starting;

import scripts.fc.fcpaint.FCPaint;
import scripts.fc.fcpaint.FCPaintable;
import scripts.fc.fcplankmaker.data.Vars;
import scripts.fc.fcplankmaker.gui.FCPlankMakerGUI;
import scripts.fc.fcplankmaker.workers.Workers;
import scripts.fc.utils.FCInventoryListener;
import scripts.fc.utils.FCInventoryObserver;
import scripts.fc.workerframework.Worker;

@ScriptManifest(
		authors     = { 
		    "Final Calibur",
		}, 
		category    = "Money Making", 
		name        = "FC Plank Maker", 
		version     = 0.1, 
		description = "Makes any plank at Varrock", 
		gameMode    = 1)

public class FCPlankMaker extends Script implements FCPaintable, Painting, Starting, Ending, FCInventoryListener
{
	//Local constants
	private final ScriptManifest 		MANIFEST = (ScriptManifest)this.getClass().getAnnotation(ScriptManifest.class); //The script manifest object
	private final FCPaint				PAINT = new FCPaint(this); //Our simple painting system
	private final long 					SLEEP_TIME = 100;	//Script sleep time
	private final FCInventoryObserver 	INV_OBSERVER = new FCInventoryObserver(); //Inventory observer so we can update planksMade
	
	//Local variables
	private FCPlankMakerGUI gui; //Our GUI
	private Worker 			currentWorker = Workers.BANK.getWorker();
	private Worker 			nextWorker;
	private int				planksMade;
	
	
	@Override
	public void run()
	{
		while(Vars.get().hasSupplies) //WHILE(we have supplies to make planks)
		{
			if(Login.getLoginState() != STATE.INGAME || Vars.get().plankType == null) //IF(not logged in OR plank type is null(gui has not been filled out))
			{
				continue; //Skip this loop iteration
			}
			
			nextWorker = currentWorker.getNextWorker(); //Grab the next worker
			
			if(nextWorker == null)
			{
				currentWorker.work();
			}
			else
			{
				currentWorker = nextWorker;
			}
			
			sleep(SLEEP_TIME); //Sleep to reduce CPU usage
		}
	}
	
	@Override
	public void onEnd()
	{
		println("Thank you for running " + MANIFEST.name() + " v" + MANIFEST.version() + " by " + MANIFEST.authors()[0]);
	}

	@Override
	public void onStart()
	{
		println("Started " + MANIFEST.name() + " v" + MANIFEST.version() + " by " + MANIFEST.authors()[0]);
		
		INV_OBSERVER.addListener(this);
		
		Vars.reset();
		
		handleGui();
	}

	@Override
	public void onPaint(Graphics g)
	{
		PAINT.paint(g);
	}

	@Override
	public String[] getPaintInfo()
	{
		return new String[]{"Time ran: " + PAINT.getTimeRan(), "Planks made: " + planksMade,
				"Planks per hour: " + PAINT.getPerHour(planksMade), "Status: " + Vars.get().status};
	}
	
	private void handleGui()
	{	
		/*
		 * Initialize GUI on the event queue
		 * 
		 * This is important so that we don't have to force our users
		 * to have the Tribot client in lite mode to run the script
		 */
		EventQueue.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					gui = new FCPlankMakerGUI();	
					gui.setLocationRelativeTo(null);
					gui.setAlwaysOnTop(true);
					
					//Position GUI in center of screen
					Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();		
					gui.setLocation((int)(screenSize.getWidth() / 2), (int)(screenSize.getHeight() / 2));
					
					//Make GUI visible
					gui.setVisible(true);
				} 
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void inventoryAdded(int id, int count)
	{
		//Null check avoids NPE errors if the user logs in and still hasn't filled the GUI out
		if(Vars.get().plankType != null && id == Vars.get().plankType.getPlankId())
		{
			planksMade += count;
		}
	}

	@Override
	public void inventoryRemoved(int id, int count)
	{}
	
}
